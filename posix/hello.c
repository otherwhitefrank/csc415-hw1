/*  
    Name: Frank Dye
    Date: 1/29/2014
    Description: A basic hello world program using low level POSIX system calls.
*/



#include <stdio.h>
#include <unistd.h>


#define MY_NAME "Frank Dye"
#define MAX_STRING_SIZE 50


int main(char* argv)
{

  char szBuffer[MAX_STRING_SIZE];
  char szGreeting[] = "Hello 415, it's me ";


  int lengthBuffer = 0; //Length of our string buffer
  ssize_t numBytesWritten = 0; //Number of bytes written useing write()

  lengthBuffer = sprintf(szBuffer, "%s%s%c%c", szGreeting, MY_NAME, '!', '\n',MAX_STRING_SIZE);

  if (write(STDOUT_FILENO, szBuffer, lengthBuffer) == -1)
  {
    //Error writing file
    printf("Error writing file!");
  
    //Exit with error
    return -1;
  }
  

  //End program

  return 0;
  
  

}
