/*  Name: Frank Dye
	Date: 1/28/2014
	Description: A basic hello world program using win32 system calls.
*/

#include <windows.h>
#include <stdio.h>


#define MY_NAME "Frank Dye"
#define MAX_STRING_SIZE 50

int main(char* argv)
{

		char szBuffer[MAX_STRING_SIZE];
		char szGreeting[] = "Hello 415, it's me ";
		
		int lengthBuffer = 0;
		
		HANDLE stdOutHandle = NULL;
		
		DWORD numBytesWritten = 0;
		
		
		
		//Build our string buffer
		lengthBuffer = sprintf(szBuffer, "%s%s%c", szGreeting, MY_NAME, '!', MAX_STRING_SIZE);
		
		//Acquire stdout
		stdOutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		
		if (stdOutHandle != INVALID_HANDLE_VALUE)
		{
			//Opening standard output worked.
			
			WriteFile(stdOutHandle, &szBuffer, lengthBuffer, &numBytesWritten, NULL);
			
			if (numBytesWritten != lengthBuffer)
			{
				printf("Error writing to standard out!");
			}
			
			//Exit program with error code 0
			return 0;
		}
		else
		{
			//Error opening standard output.
			printf("Error opening standard output!");
			
			//Exit program with error code -1
			return -1;
		}		
		
}